# SRC-UvA-plugins

Repository containing different `Surf Research Cloud` - `University of Amsterdam` plugins.

# SRC-UVA-Catalog items
Catalog items are multiple components together. An ansible playbook can manage a single component. 

This repository re-uses a template set of components and therefore a 'base catalog item'. This base catalog item contains Surf Research Cloud built plugins that set-up the Operating System, Collaboration access management and ansible execution of external plugins. 
Our plugins are the external plugins, so any plugins developed on this page are placed on top of the catalog template, yielding a new catalog item. Multiple components could be added to a single catalog item but this will be detailed in the future. 

# Current `Gotcha's` and `Aha's`

Many of these points have been figured out by reading the official documentation of [Surf Research Cloud](https://servicedesk.surf.nl/wiki/display/WIKI/SURF+Research+Cloud) and communicating with Surf advisors.

1. A component is the lowest level building block
2. A catalog-item is a collection of components
3. A catalog is a collection of catalog-items
4. You can clone a catalog-item and keep it as a template
    1. uva current template: `SRC-OS` +  `SRC-CO` + `SRC-External`
5. You can create optional, required and interactive parameters
6. Interactive parameters are used on workspace creation
7. Required parameters are on catalog-item creation, unless interactive
8. You can create a ansible 'test' machine to prevent re-creating the same workspace while developing components. 
    1. `SRC-OS` + `SRC-CO` + `SRC-External` + `dummy component` in `Ansible`
        1. Dummy block can be an empty ansible component
    2. SRC-External will install `Ansbile`
    3. Likely the same process can be repeated for `Docker compose` and `Powershell`
9. There are multiple `flavours` of vendor, listed on the [official Surf Wiki](https://servicedesk.surf.nl/wiki/display/WIKI/SRC+Available+Flavours)
